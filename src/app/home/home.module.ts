import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { ListConselingPage } from '../conseling/list-conseling/list-conseling.page';
import { SchedulerPage } from '../scheduler/scheduler.page';
import { ResultConselingPage } from '../conseling/result-conseling/result-conseling.page';
import { FormSchedulePage } from '../scheduler/form-schedule/form-schedule.page';
import { CalendarModule } from 'ion2-calendar';
import { HistoriesPage } from '../conseling/histories/histories.page';
import { LogChatPage } from '../room-chat/log-chat/log-chat.page';

@NgModule({
  entryComponents:[
    ListConselingPage,
    HistoriesPage,
    ResultConselingPage,
    SchedulerPage,
    FormSchedulePage,
    LogChatPage
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendarModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [
    HomePage,
    HistoriesPage,
    ListConselingPage,
    ResultConselingPage,
    SchedulerPage,
    FormSchedulePage,
    LogChatPage
  ]
})
export class HomePageModule {}
