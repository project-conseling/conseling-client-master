import { Component } from '@angular/core';
import { RouteService } from '../route.service';
import { FormSchedulePage } from '../scheduler/form-schedule/form-schedule.page';
import { ModalController } from '@ionic/angular';
import { Complaint } from '../_types/complaint';
import { ProvidersService } from '../services/providers.service';
import { Category } from '../_types/category';
import { RealUpdateService } from '../real-update.service';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs';
import { RTCService } from '../rtc.service';

declare var apiRTC: any;
declare var apiCC;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  /**
   * @parameter segmentValue;
   * fungsi = untuk switch case segment button pada footer main page
   * anda dapat melihatnya dalam file home.page.html terdapat ion-footer dan terdapat segment
   * 
   * dalam element ion-segment sendiri terdapat event change yang mana akan memanggil segmentChanged ketika diganti
   */
  segmentValue = "conseling";
  dataComplaint: Complaint;
  categories: Category[]
  dataConseling = [];
  


  constructor(public serviceRoute: RouteService, private modalCtrl: ModalController, private api: ProvidersService, 
    public socketApi: RealUpdateService, private socket: Socket,
    public rtcService: RTCService) {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.statusActive()
  }

  
  segmentChanged($event) {
    this.segmentValue = $event.detail.value;
    // this.openModal()
  }

  // async openModal() {
  //   if(this.segmentValue == 'schedule') {
  //     let data;
  //     let storeLocal = localStorage.getItem('_USER');
  //     let id = JSON.parse(storeLocal)._ID;
  //     this.api.getPatientConseling(id)
  //     .subscribe(async (myConseling: any) => {
  //       if(myConseling && myConseling.length > 0 ) {
  //         await myConseling.forEach(consel => {
  //           if(consel.result == null) {
  //             data = consel
  //           }
  //         })
  //       }
  //       await this.api.getPatientComplaint(id)
  //       .subscribe(async (resp: any) => {
  //         this.dataComplaint = await resp
  //         const modal = await this.modalCtrl.create({
  //           component: FormSchedulePage,
  //           componentProps: {
  //             detail: this.dataComplaint,
  //             scheduler: data
  //           }
  //         });
  //         return await modal.present();
  //       })
  //     })

  //   }
  // }

  getActiveUser() {
    let observable = new Observable(obs => {
      this.socket.on('users-changed', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  statusActive() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.socket.emit('set-nickname', id)
    setTimeout(() => {
      this.statusActive()
    }, 3000)
  }


  /**
   * apiRTC
   */
  
}
