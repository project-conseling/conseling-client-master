import { Injectable } from '@angular/core';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RealUpdateService {
  statusConselor = 0;

  constructor(private socket: Socket) {
    
   }

  getUpdate(){
    let observable = new Observable(obs => {
      this.socket.on('status-changed', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  getMessages() {
    let observable = new Observable(obs => {
      this.socket.on('message', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  getActiveUser() {
    let observable = new Observable(obs => {
      this.socket.on('users-changed', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }
}
