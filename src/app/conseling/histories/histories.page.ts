import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProvidersService } from 'src/app/services/providers.service';
import { Category } from 'src/app/_types/category';
import { MyProfile } from 'src/app/_types/my.profile';
import { ResultConselingPage } from '../result-conseling/result-conseling.page';
import { LogChatPage } from 'src/app/room-chat/log-chat/log-chat.page';

@Component({
  selector: 'app-histories',
  templateUrl: './histories.page.html',
  styleUrls: ['./histories.page.scss'],
})
export class HistoriesPage implements OnInit {
  conseling: any[];
  categories = []
  category: any;
  myProfile: MyProfile;
  onload: boolean = false;
  formSearch = ''
  tempConseling: any[]
  constructor(private api: ProvidersService, private modalCtrl: ModalController) {
    this.onload = true;
  }

  ngOnInit() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.api.getPatientConseling(id)
      .subscribe( async (conselings: any) => {
        this.conseling = []
        if(conselings.length > 0) {
          await conselings.map((element, index) => {
            element.counter = (index + 1)
            console.log(element.counter)
            if(element.status == 8) {
              console.log(element)
              this.conseling.push(element)
            }
          });
        }
        this.tempConseling = this.conseling
        // await this.api.getCategories()
        // .subscribe((respCat: any) => {
        //   this.categories = respCat.data
        // })
    })

    // this.api.getPatientComplaint(id)
    // .subscribe(async (resp: any) => {
    //   this.myProfile = resp.profile
      
      setTimeout(() => {
        this.onload = false
      }, 1500)
    // })
    
  }

  async openResult(data) {
    const modal = await this.modalCtrl.create({
      component: ResultConselingPage,
      componentProps: {
        dataConseling: data,
        categories: this.categories
      }
    });
    modal.onDidDismiss().then((res: any) => {
      if(res.data) {
        data = res.data
      }
    })
    return await modal.present();
    
  }

  doSearch(){
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    console.log(this.formSearch)
    this.onload = true
    this.conseling = []
    if(this.formSearch != '') {
      try {
        this.api.searchPatientConseling(id, {textSearch: this.formSearch})
        .subscribe((conselings: any) => {
          console.log("Hasil Pencarian")
            if(conselings.length > 0) {
              
              conselings.map((element, index) => {
                element.counter = (index + 1)
                console.log(element.counter)
                if(element.status == 8) {
                  console.log(element)
                  this.conseling.push(element)
                }
              });
            }
        })
      } catch (error) {
        console.log(error)
      } finally {
        setTimeout(() => {
          this.onload = false
        }, 1000)
      }
    } else {
      try {
        this.conseling = this.tempConseling
      } catch (error) {
        console.log(error)
      } finally {
        this.onload = false
      }
    }
  }

  async openLogChat(data) {
    let msgList = []
    console.log(data)
    data.chat_rooms.forEach(chat => {
      let chipTime = chat.createdAt.split('T')[1].split(":")[0]+ ":" + chat.createdAt.split('T')[1].split(":")[1]
      let chatData = {
        userId: chat.user_id,
        userName: chat.user.profile.name,
        userAvatar: chat.user.profile.avatar,
        time: chipTime,
        message: chat.chat,
        isReaded: 1,
        id: chat.id
      }
      msgList.push(chatData)
    });
    const modal = await this.modalCtrl.create({
      component: LogChatPage,
      componentProps: {
        msgList: msgList,
        toUser: data.conselor_id
      }
    });
    return await modal.present();
  }

}
