import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-result-conseling',
  templateUrl: './result-conseling.page.html',
  styleUrls: ['./result-conseling.page.scss'],
})
export class ResultConselingPage implements OnInit {
  @Input() dataConseling: any;
  category: string;
  @Input() categories = []
  onload: boolean = false;
  mon: string;
  day: string;
  year: any;
  constructor(private modalCtrl: ModalController, private api: ProvidersService) { }
  
  ngOnInit() {
    this.onload = true;
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    console.log("dataConseling", this.dataConseling)
    // this.api.getComplaintId(this.dataConseling.complaint_id)
    // .subscribe( async (resp: any) => {
    //   await this.api.getScheduleConseling(this.dataConseling._id)
    //   .subscribe(async (conseling: any) => {
    //     this.dataConseling.schedule = conseling.data[0].date + " " + conseling.data[0].time
    //     await this.api.getProfile(id)
    //     .subscribe(async (profile: any) => {
    //       let tempProfile = await profile.data[0]
    //       this.dataConseling.profile = await tempProfile
    //       this.category = await this.getCategory(resp.data.category_id)
    //       setTimeout(() => {
    //         this.onload = false;
    //       }, 1000)
    //     })
    //   })
      
    // })
  }

  getCategory(id) {
    let category;
    this.categories.forEach(element => {
      if(element._id == id) {
        category = element.category
      }
    });
    return category;  
  }  

  closeModal() {
    this.modalCtrl.dismiss()
  }

  dateTxt(data) {
    let dtDate: Date;
    dtDate = new Date(data)
    this.getDay(dtDate.getDay())
    this.year = dtDate.getFullYear();
    switch (dtDate.getMonth()) {
      case 0:
        this.mon = "Januari"
        break;
      case 1:
        this.mon = "Februari"
        break;
      case 2:
        this.mon = "Maret"
        break;
      case 3:
        this.mon = "April"
        break;
      case 4:
        this.mon = "Mei"
        break;
      case 5:
        this.mon = "Juni"
        break;
      case 6:
        this.mon = "Juli"
        break;
      case 7:
        this.mon = "Agustus"
        break;
      case 8:
        this.mon = "September"
        break;
      case 9:
        this.mon = "Oktober"
        break;
      case 10:
        this.mon = "November"
        break;
      case 11:
        this.mon = "Desember"
        break;
      default:
        break;
    }
  }

  getDay(day) {
    switch (day) {
      case 1:
        this.day = "Senin"
        break;
      case 2:
        this.day = "Selasa"
        break;
      case 3:
        this.day = "Rabu"
        break;
      case 4:
        this.day = "Kamis"
        break;
      case 5:
        this.day = "Jumat"
        break;
      case 6:
        this.day = "Sabtu"
        break;
      case 7:
        this.day = "Minggu"
        break;
      default:
        break;
    }
  }

}
