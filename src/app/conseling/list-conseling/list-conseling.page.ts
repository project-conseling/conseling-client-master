import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProvidersService } from 'src/app/services/providers.service';
import { Category } from 'src/app/_types/category';
import { MyProfile } from 'src/app/_types/my.profile';
import { ResultConselingPage } from '../result-conseling/result-conseling.page';

@Component({
  selector: 'app-list-conseling',
  templateUrl: './list-conseling.page.html',
  styleUrls: ['./list-conseling.page.scss'],
})
export class ListConselingPage implements OnInit {
  conseling: any[];
  categories = []
  category: any;
  myProfile: MyProfile;
  onload: boolean = false;
  tempConseling: any[]
  constructor(private api: ProvidersService, private modalCtrl: ModalController) {
    this.onload = true;
  }

  ngOnInit() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.api.getPatientConseling(id)
      .subscribe( async (conselings: any) => {
        console.log(conselings)
        this.conseling = []
        if(conselings.length > 0) {
          conselings.forEach(element => {
            if(element.status == 0) {
              this.conseling.push(element)
            }
          });
          this.tempConseling = this.conseling
        }
        // await this.api.getCategories()
        // .subscribe((respCat: any) => {
        //   this.categories = respCat.data
        // })
    })

    // this.api.getPatientComplaint(id)
    // .subscribe(async (resp: any) => {
    //   this.myProfile = resp.profile
      
      setTimeout(() => {
        this.onload = false
      }, 1500)
    // })
    
  }

  async openResult(data) {
    const modal = await this.modalCtrl.create({
      component: ResultConselingPage,
      componentProps: {
        dataConseling: data,
        categories: this.categories
      }
    });
    modal.onDidDismiss().then((res: any) => {
      if(res.data) {
        data = res.data
      }
    })
    return await modal.present();
  }

}
