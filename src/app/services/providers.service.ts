import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment'
import { Observable } from 'rxjs';
import { Category } from '../_types/category';
import { MyProfile } from '../_types/my.profile';
const ENV = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(private httpClient: HttpClient) { }

  getCategories(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(`${ENV}/category`)
  }

  /**
   * USER API
   * 
   */
  getProfile(id): Observable<MyProfile> {
    return this.httpClient.get<MyProfile>(`${ENV}/profile/${id}`);
  }


  /**
   * Complain API
   */

  /**
   * getPatientComplaint
   * @param id merupakan id remaja. 
   */
  getPatientComplaint(id) {
    return this.httpClient.get(`${ENV}/complaint/patient/${id}`);
  }
  /**
   * getComplaintId
   * @param id merupakan id table complaint. 
   */
  getComplaintId(id){
    return this.httpClient.get(`${ENV}/complaint/id/${id}`);
  }
  postComplain(req) {
    return this.httpClient.post(`${ENV}/complaint`,req);
  }
  updateComplaint(req) {
    return this.httpClient.put(`${ENV}/complaint/${req._id}`,req);
    
  }

  /**
   * Conseling API
   */
  getPatientConseling(id) {
    return this.httpClient.get(`${ENV}/conseling/patient/${id}`);
  }

  searchPatientConseling(id, formSearch) {
    return this.httpClient.post(`${ENV}/conseling/search/${id}`, formSearch);
  }
  createConseling(req) {
    return this.httpClient.post(`${ENV}/conseling`, req)
  }
  updateStatusComplaintToConsult(req) {
    let form = {
      _id: req._id,
      status: 9,
      title: req.title,
      description: req.description,
      scheduleId: req.scheduleId,
      patientId: req.patientId,
      conselorId: req.conselorId,
      created_on: req.created_on
    }
    return this.httpClient.put(`${ENV}/complaint/${req._id}`,form);
  }
  updateConseling(req) {
    return this.httpClient.put(`${ENV}/conseling/${req.id}`, req);
  }

  modifyConseling(req) {
    return this.httpClient.put(`${ENV}/conseling/update/${req.id}`, req)
  }


  /**
   * Schedul API
   */

  getPatientSchedule(id) {
    return this.httpClient.get(`${ENV}/schedule/conselings/patient/${id}`);
  }
  getScheduleConseling(id) {
    return this.httpClient.get(`${ENV}/schedule/conselings/${id}`);
  }
  getScheduleConselor(id) {
    return this.httpClient.get(`${ENV}/schedule/me/${id}`);
  }
  getWeekly(id) {
    return this.httpClient.get(`${ENV}/schedule/weekly/${id}`)
  }
  getScheduleByDate(date) {
    return this.httpClient.get(`${ENV}/schedule/${date}`);
  }
  updateSchedule(req) {
    return this.httpClient.put(`${ENV}/schedule/id/${req._id}`, req);
  }

  setAvailable(id) {
    return this.httpClient.put(`${ENV}/schedule/set-available/${id}`, {id: id})
  }

  setUnAvailable(id) {
    return this.httpClient.put(`${ENV}/schedule/set-unavailable/${id}`, {id: id})
  }


  getChat(id) {
    return this.httpClient.get(`${ENV}/log/chat/${id}`);
  }
  sendChat(req) {
    return this.httpClient.post(`${ENV}/chat`, req)
  }

  postQuestion(req) {
    return this.httpClient.post(`${ENV}/question`, req)
  }
  
  getListQuestion() {
    return this.httpClient.get(`${ENV}/question`)
  }

  searchQuestion(req) {
    return this.httpClient.post(`${ENV}/question/search`, {textSearch: req})
  }
}
