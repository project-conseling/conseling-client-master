import { Component, OnInit } from '@angular/core';
import { ProvidersService } from '../services/providers.service';
import { Complaint } from '../_types/complaint';
import { FormSchedulePage } from './form-schedule/form-schedule.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-scheduler',
  templateUrl: './scheduler.page.html',
  styleUrls: ['./scheduler.page.scss'],
})
export class SchedulerPage implements OnInit {
  mon: string;
  monInt = null;
  day: string;
  year: any;
  dt = [null, null, null, null, null, null, null,]
  onload: boolean = false;
  firstTime = true;
  scheduler: any;
  conseling: any;
  dataComplaint: Complaint;
  methode: any;
  constructor(private api: ProvidersService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.onload = true;
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    this.api.getPatientConseling(id)
    .subscribe(async (myConseling: any) => {
      console.log('conselings => ', myConseling)
      if(myConseling && myConseling.length > 0 ) {
        await myConseling.forEach(consel => {
          if(consel.result == null) {
            this.scheduler = consel
          }
        })
      }
      console.log(this.scheduler)
      this.api.getPatientComplaint(id)
      .subscribe(async (resp: any) => {
        this.dataComplaint = resp
        if(this.scheduler){
          this.dateTxt(this.scheduler.schedule.schedule_date)
          console.log(this.mon)
        }
        setTimeout(() => {
          this.onload = false
          if(!this.scheduler) {
            this.openModal()
          } else {
            this.firstTime = false
          }
        }, 1500)
      })
    })
    
  }

  dateTxt(data) {
    let dtDate: Date;
    dtDate = new Date(data)
    let OneDayAgo = new Date()
    let TwoDayAgo = new Date()
    let ThreeDayAgo = new Date()

    let OneDayLatter = new Date()
    let TwoDayLatter = new Date()
    let ThreeDayLatter = new Date()

    OneDayAgo.setDate(dtDate.getDate()-1)
    TwoDayAgo.setDate(dtDate.getDate()-2)
    ThreeDayAgo.setDate(dtDate.getDate()-3)

    OneDayLatter.setDate(dtDate.getDate()+1)
    TwoDayLatter.setDate(dtDate.getDate()+2)
    ThreeDayLatter.setDate(dtDate.getDate()+3)

    this.getDay(dtDate.getDay())
    this.year = dtDate.getFullYear();
    this.monInt = dtDate.getMonth();
    console.log(dtDate.getDate())
    console.log("Now => ", dtDate)
    console.log("one Ago => ", OneDayAgo)

    this.dt[0] = ThreeDayAgo.getDate()
    this.dt[1] = TwoDayAgo.getDate()
    this.dt[2] = OneDayAgo.getDate()
    this.dt[3] = dtDate.getDate()
    this.dt[4] = OneDayLatter.getDate()
    this.dt[5] = TwoDayLatter.getDate()
    this.dt[6] = ThreeDayLatter.getDate()

    switch (dtDate.getMonth()) {
      case 0:
        this.mon = "Januari"
        break;
      case 1:
        this.mon = "Februari"
        break;
      case 2:
        this.mon = "Maret"
        break;
      case 3:
        this.mon = "April"
        break;
      case 4:
        this.mon = "Mei"
        break;
      case 5:
        this.mon = "Juni"
        break;
      case 6:
        this.mon = "Juli"
        break;
      case 7:
        this.mon = "Agustus"
        break;
      case 8:
        this.mon = "September"
        break;
      case 9:
        this.mon = "Oktober"
        break;
      case 10:
        this.mon = "November"
        break;
      case 11:
        this.mon = "Desember"
        break;
      default:
        break;
    }
  }

  getDay(day) {
    switch (day) {
      case 1:
        this.day = "Senin"
        break;
      case 2:
        this.day = "Selasa"
        break;
      case 3:
        this.day = "Rabu"
        break;
      case 4:
        this.day = "Kamis"
        break;
      case 5:
        this.day = "Jumat"
        break;
      case 6:
        this.day = "Sabtu"
        break;
      case 7:
        this.day = "Minggu"
        break;
      default:
        break;
    }
  }

  getterDay(day) {
    let getter
    switch (day) {
      case 1:
        getter = "Senin"
        break;
      case 2:
        getter = "Selasa"
        break;
      case 3:
        getter = "Rabu"
        break;
      case 4:
        getter = "Kamis"
        break;
      case 5:
        getter = "Jumat"
        break;
      case 6:
        getter = "Sabtu"
        break;
      case 7:
        getter = "Minggu"
        break;
      default:
        break;
    }
    return getter
  }

  async openModal() {
    console.log(this.dataComplaint)
    const modal = await this.modalCtrl.create({
      component: FormSchedulePage,
      componentProps: {
        detail: this.dataComplaint
      }
    });
    modal.onDidDismiss().then(() => {
      this.ngOnInit()
    })
    return await modal.present();
  }

  async editSchedule() {
    const modal = await this.modalCtrl.create({
      component: FormSchedulePage,
      componentProps: {
        detail: this.dataComplaint,
        sc: this.scheduler
      }
    });
    modal.onDidDismiss().then(() => {
      this.ngOnInit()
    })
    return await modal.present();
  }

}
