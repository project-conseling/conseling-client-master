import { Component, OnInit, Input } from '@angular/core';
import { ConselingServiceService } from 'src/app/services/conseling-service.service';
import { ModalController } from '@ionic/angular';
import { async } from 'q';
import { ProvidersService } from 'src/app/services/providers.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar';

@Component({
  selector: 'app-form-schedule',
  templateUrl: './form-schedule.page.html',
  styleUrls: ['./form-schedule.page.scss'],
})
export class FormSchedulePage implements OnInit {
  @Input() detail: any;
  @Input() scheduler: any;
  @Input() sc: any;
  
  scheduleConselor = [];
  weeklyConselor = [];
  timings = [];
  formSchedule = {
    date: "",
    time: "",
    conselor_id: "",
    patient_id: ""
  }
  formConseling = {
    complaint_id: "",
    methode: "",
    option: "",
    result: "",
    patientId: "",
    conselorId: "",
  }
  day = "";
  clocks = [
    {clock:'08:00', avail: 1},{clock:'09:00', avail: 1},{clock:'10:00', avail: 1},
    {clock:'11:00', avail: 1},{clock:'12:00', avail: 1},{clock:'13:00', avail: 1},
    {clock:'14:00', avail: 1},{clock:'15:00', avail: 1},{clock:'16:00', avail: 1},
    {clock:'17:00', avail: 1},{clock:'18:00', avail: 1},{clock:'19:00', avail: 1}
  ];
  formApply = {
    applyDate: '',
    clock: ''
  }

  schedule = [];
  times = []
  days = "";


  optionsCalendar: CalendarComponentOptions = {
    showMonthPicker: false,
    weekdays: ['minggu','senin','selasa','rabu','kamis','jumat','sabtu'],
    monthFormat: 'MMMM YYYY',
    daysConfig: []

  };

  selectedDate = ''
  selectedTime = ""
  onLoad = true;
  constructor(private api: ProvidersService, private modalCtrl: ModalController, private localNotifications: LocalNotifications ) { }

  ngOnInit() {
    this.onLoad = true
    console.log(this.scheduler)
    console.log(this.detail)
    let nowDate = new Date();
    
    let _daysConfig: DayConfig[] = [];
    this.selectedDate = nowDate.getFullYear()+"-"+this.pad(nowDate.getMonth()+1, 2)+"-"+this.pad(nowDate.getDate(), 2)
    
    try {
      this.api.getScheduleConselor(this.detail.conselor.id)
      .subscribe((resp: any) => {
        this.schedule = resp
        console.log(resp)
          resp.forEach(schedule => {
            _daysConfig.push(
              {
                date: new Date(schedule.schedule_date),
                marked: true
              }
            )
            let text = ""
            console.log(schedule)
            if(this.selectedDate == schedule.schedule_date && schedule.status == 0) {
              this.times.push(
                {time: schedule.time, text: this.getTime(schedule.time)})
            }
          })
          console.log(_daysConfig)
          this.optionsCalendar.daysConfig = _daysConfig;
      })
    } catch (error) {
      
    } finally {
      setTimeout(() => {
        this.onLoad = false
      }, 2000)
    }
  }

  getTime(time) {
    let min = time.split(":")[1]
    let hour = time.split(":")[0]
    if((parseInt(min) + 90) > 60 ) {
      hour = parseInt(hour) + 1
      min = (parseInt(min) + 90) - 60
      if(min >= 60) {
        hour = hour + 1
        min = min - 60
      }
    }
    return this.pad(parseInt(hour), 2) +":"+ this.pad(parseInt(min), 2)
  }
  formatDate() {
    var d = new Date();
    
    var month = '' + (d.getMonth() + 1);
    var day = '' + d.getDate();
    var year = d.getFullYear();
    var hari = d.getDay()
    console.log(hari)
    let dayName;
    switch (hari) {
      case 1:
        dayName = "Senin"
        break;
      case 2:
        dayName = "Selasa"
        break;
      case 3:
        dayName = "Rabu"
        break;
      case 4:
        dayName = "Kamis"
        break;
      case 5:
        dayName = "Jumat"
        break;
      default:
        break;
    }
    console.log(dayName)
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    this.day = dayName
    return [year, month, day].join('-');
  }

  closeModal() {
    this.modalCtrl.dismiss()
  }
  checkSchedules() {
    this.times = []
    console.log(this.formSchedule.date)
    this.schedule.forEach(element => {
      if(element.date == this.formSchedule.date) {
        element.time.forEach(time => {
          if(time.status != 0) {
            switch (time.time) {
              case "09:00":
                time.text = "09:00-10:00";
                break;
              case "10:00":
                time.text = "10:00-11:00";
                break;
              case "11:00":
                time.text = "11:00-12:00";
                break;
              case "12:00":
                time.text = "12:00-13:00";
                break;
              case "13:00":
                time.text = "13:00-14:00";
                break;
              case "14:00":
                time.text = "14:00-15:00";
                break;
              case "15:00":
                time.text = "15:00-16:00";
                break;
              case "16:00":
                time.text = "16:00-17:00";
                break;
              case "17:00":
                time.text = "17:00-18:00";
                break;
              case "18:00":
                time.text = "18:00-19:00";
                break;
              case "19:00":
                time.text = "19:00-20:00";
                break;
              case "20:00":
                time.text = "20:00-21:00";
                break;
              case "21:00":
                time.text = "21:00-22:00";
                break;
              case "22:00":
                time.text = "22:00-23:00";
                break;
              default:
                break;
            }
            this.times.push(time)
          }
        });
      }
    });
  }
  checkSchedule() {
    this.times = []
    this.schedule.forEach(schedule => {
      let text = ""
      if(this.selectedDate == schedule.schedule_date && schedule.status == 0) {
        
        this.times.push(
          {time: schedule.time, text: schedule.time +" s/d "+ this.getTime(schedule.time)})
      }
    })
  }

  calendarChanged($event){
    console.log($event)
  }

  async doPost() {
    let schedule_id
    let sc = this.schedule[this.schedule.findIndex(val => val.schedule_date == this.selectedDate && val.time == this.formSchedule.time)]
    let form = {
      conselor_id: this.detail.conselor.id,
      status: 0,
      methode: this.formConseling.methode,
      patient_id: this.detail.patient.id,
      schedule_id: sc.id
    }
    if(this.sc) {
      this.doUpdate()
    } else {
      this.api.createConseling(form).subscribe( async (res: any) => {
          if(res && res.code && res.code == 200) {
            let times = new Date(this.selectedDate).getTime() - 6400000
            // if(times < new Date().getTime()) {
            //   times = new Date(this.formSchedule.date).getTime()
            // } 
            this.localNotifications.schedule({
              id: res.id,
              title: 'Peringatan!',
              text: 'Anda memiliki jadwal konseling, jangan sampai terlewatkan. silahkan menunggu 10 Menit sebelum jam yang telah di tentukan',
              data: { mydata: 'Anda Memiliki konsultasi' },
              trigger: { at: new Date(new Date(times)) },
            });
          }
          this.closeModal()
      })
    }
  }

  async doUpdate() {
    let sc = this.schedule[this.schedule.findIndex(val => val.schedule_date == this.selectedDate && val.time == this.formSchedule.time)]
    this.api.setAvailable(this.sc.schedule_id)
    .subscribe(avail => {
      console.log("avail => ", avail)
      let updatePayload = {
        schedule_id: sc.id,
        methode: this.formConseling.methode,
        id: this.sc.id
      }
      this.api.setUnAvailable(sc.id)
      .subscribe(unavail => {
        this.api.modifyConseling(updatePayload)
        .subscribe((resp: any) => {
          console.log("update => ", resp)
          let times = new Date(this.selectedDate).getTime() - 6400000
          // if(times < new Date().getTime()) {
          //   times = new Date(this.formSchedule.date).getTime()
          // } 
          this.localNotifications.schedule({
            id: resp.id,
            title: 'Peringatan!',
            text: 'Anda memiliki jadwal konseling, jangan sampai terlewatkan. silahkan menunggu 10 Menit sebelum jam yang telah di tentukan',
            data: { mydata: 'Anda Memiliki konsultasi' },
            trigger: { at: new Date(new Date(times)) },
          });
          this.closeModal()
        })
      })
    })
  }
  pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }
}
