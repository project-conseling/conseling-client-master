import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { CategoryService } from 'src/app/services/category.service';
import { ComplaintService } from 'src/app/services/complaint.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesService } from 'src/app/user/services.service';
import { ProvidersService } from 'src/app/services/providers.service';
import { UserService } from 'src/app/user/user.service';
import { IonContent, NavController, Events } from '@ionic/angular';
import { Socket } from 'ng-socket-io';

@Component({
  selector: 'app-apply-form',
  templateUrl: './apply-form.page.html',
  styleUrls: ['./apply-form.page.scss'],
})
export class ApplyFormPage implements OnInit {
  @ViewChild('IonConten') content: IonContent
  categories: any;
  labelCategory = "Pilih Kategori";
  formComplaint = {
    category: '',
    information: '',
    story: '',
  }
  customActionSheetOptions: any = {
    header: 'Jenis Kekerasan *',
  };
  afterRegistrasi = false;
  constructor(private api: ProvidersService, public events: Events, private socket: Socket,
    private router: Router, private userApi: UserService, private activatedRoute: ActivatedRoute, private navCtrl: NavController) {
      events.subscribe('user:created', (form) => {
        // user and time are the same arguments passed in `events.publish(user, time)`
        console.log('Welcome', form);
      });
    }

  ngOnInit() {
    console.log(localStorage.getItem("bulk_regist"))
    if(this.activatedRoute.snapshot.paramMap.get('slug') == "regist") {
      this.afterRegistrasi = true;
    }
    this.api.getCategories()
    .subscribe((res: any) => {
      this.categories = res.data
    })
  }

  selectedCategory($event) {
    let id = $event.detail.value;
    this.categories.forEach(category => {
      if(id == category._id){
        this.formComplaint.category = category._id
        this.labelCategory = category.category,
        this.formComplaint.information = category.information
      }
    });
  }

  async doPost() {
    let toastLogin
    try {
      await this.userApi.register(JSON.parse(localStorage.getItem("bulk_regist")))
      .subscribe((resp: any) => {
        console.log(resp)
        if(resp && resp.code == 409)  {
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: resp.msg
          })
          
        } else { 
          toastLogin = Swal.mixin({
            title: '<strong>Mohon Tunggu</strong>',
            // text: 'Sedang dalam proses registrasi',
            timer: 3000,
          })
          toastLogin.fire({
            onBeforeOpen: () => {
              // toastRegister.resumeTimer()
              toastLogin.showLoading();
              // timerInterval = setInterval(() => {
              // }, 3000)
            },
            onOpen: async () => {
              toastLogin.stopTimer();
              await this.userApi.login(JSON.parse(localStorage.getItem("bulk_login"))).subscribe(async (res :any) => {
                let user = res;
                if(res && res.token) {
                  if(res.id) {
                    await localStorage.setItem("_USER", JSON.stringify({_ID: user.id, role:user.role, status:user.status}));
                    await localStorage.setItem("_EMAIL", user.email)

                    await localStorage.removeItem('bulk_login')
                    await localStorage.removeItem('bulk_regist')
                  }
                } 
                console.log(res);
                
              });
              await setTimeout(() => {
                
              }, 3000)
              toastLogin.resumeTimer();
            },
            onClose: async () => {
              // clearInterval(timerInterval)
              let storeLocal = await localStorage.getItem('_USER');
              let id = await JSON.parse(storeLocal)._ID;
              let form = {
                category_id: null,
                description: this.formComplaint.story,
                patient_id: id
              }
              let delay = 2000
              let timerInterval = null
              let Error;
              Swal.fire({
                title: '<strong>Mohon Tunggu</strong>',
                timer: delay,
                onBeforeOpen: () => {
                  Swal.showLoading();
                  timerInterval = setInterval(() => {
                  }, 3000)
                },
                onOpen: async () => {
                  Swal.stopTimer();
                  await this.api.postComplain(form)
                  .subscribe(async res => {
                    await this.socket.emit('post-complaint', {status: true})
                    console.log(res)
                    if(!res) {
                      Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: "Maaf terjadi kesalahan pada server, silahkan coba beberapa saat lagi"
                      })
                    } else {
                      this.userApi.getUser(id).subscribe(async (res: any) => {
                        res.data.status = await 4;
                        this.userApi.updateUser(res.data).subscribe(async (resp: any) => {
                          await localStorage.removeItem('_USER')
                          let updated = JSON.parse(storeLocal)
                          updated.status = 4;
                          await localStorage.setItem('_USER', JSON.stringify(updated))
                        })
                      })
                      
                      Swal.resumeTimer();
                      
                      
                    }
                    
                  })
                },
                onClose: () => {
                  
                  this.router.navigateByUrl('waiting-status')
                }
              })
              
            }
          })
        }
      });
    } catch (error) {
      alert(error)
    } finally {
      
    }
    
    console.log(this.formComplaint)
  }

  doBack() {
    if(localStorage.getItem("_USER")) {
      localStorage.clear()
    }
    this.navCtrl.back()
  }

  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(1000)
    }, 1000)
  }

}
