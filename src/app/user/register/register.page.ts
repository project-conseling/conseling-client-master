import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import Swal from 'sweetalert2';
import { ActionSheetController, NavController, Events } from '@ionic/angular';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { RouteService } from 'src/app/route.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  formRegist = {
    name: '',
    email: '',
    password: '',
    gender: 'men',
    birth: '',
    address: '',
    avatar: '',
    ktp:'',
    confirmPassword: '',
    role: 0,
    age: null,
    agama: "",
    marital_status: "",
    birth_place: "",
    job: ""
  }
  bod: '';
  image: any;

  ktpName = ''
  fotoName = ''

  urlPhoto = ''
  urlKtp = ''
  idPhoto: any;
  idKtp: any;
  constructor(private actionSheetCtrl: ActionSheetController, private camera: Camera, private cloudinary: CloudinaryService,
    public formBuilder: FormBuilder, private api: UserService, public routeService: RouteService, public navCrtl: NavController,
    public events: Events) { }

  ngOnInit() {
  }

  openCalendar() {
    $('#cal-hide').click() 
  }
  normalizeDate() {
    let date = this.formRegist.birth
    let dateChiper = date.split("T")
    let dateArr = dateChiper[0].split("-");
    let dateText;
    switch (dateArr[1]) {
      case "01":
        dateText = dateArr[2]+" Januari "+dateArr[0]
        break;
      case "02":
        dateText = dateArr[2]+" Februari "+dateArr[0]
        break;
      case "03":
        dateText = dateArr[2]+" Maret "+dateArr[0]
        break;
      case "04":
        dateText = dateArr[2]+" April "+dateArr[0]
        break;
      case "05":
        dateText = dateArr[2]+" Mei "+dateArr[0]
        break;
      case "06":
        dateText = dateArr[2]+" Juni "+dateArr[0]
        break;
      case "07":
        dateText = dateArr[2]+" Juli "+dateArr[0]
        break;
      case "08":
        dateText = dateArr[2]+" Agustus "+dateArr[0]
        break;
      case "09":
        dateText = dateArr[2]+" September "+dateArr[0]
        break;
      case "10":
        dateText = dateArr[2]+" Oktober "+dateArr[0]
        break;
      case "11":
        dateText = dateArr[2]+" November "+dateArr[0]
        break;
      case "12":
        dateText = dateArr[2]+" Desember "+dateArr[0]
        break;
      default:
        break;
    }
    this.bod = dateText;
  }

  async selectOptionGetImage(imgFor: 'ktp' | 'pp') {
    const actionSheet = await this.actionSheetCtrl.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.PHOTOLIBRARY, imgFor);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.CAMERA, imgFor);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
  }

  openCam(sourceType: PictureSourceType, imgFor: 'ktp' | 'pp'){

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: sourceType,
      mediaType: this.camera.MediaType.PICTURE
    }
    

    this.camera.getPicture(options).then((imageData) => {
     this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
     this.startUpload(imageData, imgFor);
    }, (err) => {
     // Handle error
     alert("error "+JSON.stringify(err))
    });

  }

  startUpload(imgEntry, imgFor: 'ktp' | 'pp') {
    let date = new Date()
    if(imgFor == 'ktp') {
      this.ktpName = 'ktp-'+date.getTime()+'.jpg';
    } else if(imgFor == 'pp') {
      this.fotoName = 'profile-'+date.getTime()+'.jpg'
    }
    window['resolveLocalFileSystemURL'](imgEntry,
      entry => {
        entry['file']((file: any) => {
          // if(imgFor == 'ktp') {
          //   this.ktpName = file.name;
          // } else if(imgFor == 'pp') {
          //   this.fotoName = file.name
          // }
          this.readFile(file, imgFor)
        });
      });
  }

  async readFile(file: any, imgFor: 'ktp' | 'pp') {
    let folder;
    let date = new Date()
    if(imgFor == 'ktp') {
      // this.ktpName = 'ktp-'+date.getTime()+'.jpg';
      folder = 'ktp'
    } else if(imgFor == 'pp') {
      // this.fotoName = 'profile-'+date.getTime()+'.jpg'
      folder = 'profile'
    }
    
      
    const reader = new FileReader();
    reader.onloadend = async () => {
      Swal.fire({
        title: 'Mohon tunggu',
        text: 'upload file',
        timer: 3000,
        onBeforeOpen: async () => {
          Swal.showLoading();
          if(imgFor == 'ktp') {
            // this.ktpName = await file.name;
            folder = 'ktp'
          } else if(imgFor == 'pp') {
            // this.fotoName = await file.name
            folder = 'profile'
          }
        },
        onOpen: async () => {
          Swal.stopTimer();
          const formData = new FormData();
          const imgBlob = new Blob([reader.result], {type: file.type});
          formData.append('file', imgBlob, file.name);
          formData.append('folder', folder)
          formData.append('upload_preset', 'c2y6kxd3')
          await this.cloudinary.upload(formData)
          .subscribe( async (resp: any) => {
            
            if(imgFor == 'ktp') {
              this.urlKtp = resp.url
              this.idKtp = resp.public_id
              this.formRegist.ktp = resp.url
            } else if(imgFor == 'pp') {
              this.urlPhoto = resp.url
              this.idPhoto = resp.public_id
              this.formRegist.avatar = resp.url
            }
            setTimeout(() => {
              Swal.resumeTimer()
            }, 3000)
          })
        }
      })
    };
    await reader.readAsArrayBuffer(file);
  }

  checkUsia() {
    if(this.formRegist.age < 0) {
      this.formRegist.age = 0
    }
  }
  /**
   *  Validasi Inputan User
   */

  checkRegist() {
    let msgErr = '';
    let validations_form = this.formBuilder.group({
      name: new FormControl(this.formRegist.name, Validators.required),
      email: new FormControl(this.formRegist.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      password: new FormControl(this.formRegist.password, Validators.compose([
        Validators.minLength(6)])),
    });
    if(!validations_form.controls.name.valid) {
      msgErr += "<small style='color:red'>* Nama tidak boleh kosong</small><br>"
    }
    if(!validations_form.controls.email.valid) {
      msgErr += "<small style='color:red'>* Email tidak boleh kosong atau inputan tidak sesuai dengan format Email</small></br>";
    }
    if(!validations_form.controls.password.valid) {
      msgErr += "<small style='color:red'>* Password minimal 6 karakter</small></br>"
    }  
    if(validations_form.valid) {
      let form = {
        email: this.formRegist.email,
        name: this.formRegist.name,
        birth: this.formRegist.birth,
        address: this.formRegist.address,
        gender: this.formRegist.gender,
        avatar: this.formRegist.avatar,
        ktp: this.formRegist.ktp,
        password: this.formRegist.password,
        confirmPassword: this.formRegist.confirmPassword,
        role: this.formRegist.role,
        age: this.formRegist.age,
        status: 4,
        agama: this.formRegist.agama,
        marital_status: this.formRegist.marital_status,
        birth_place: this.formRegist.birth_place,
        job: this.formRegist.job
      }
      
      this.doRegist(form);
      console.log(form)
    } else {
      Swal.fire({
        title: 'Opss..',
        type: 'error',
        html: msgErr,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        showConfirmButton: false,
        cancelButtonAriaLabel: 'Tutup',
      })
      console.log("Form = "+ false)
    }
    console.log(validations_form)
  }

  /** PROSES REGISTRASI */
  doRegist(form) {
    let timerInterval
    let formLogin = {
      email: this.formRegist.email,
      password: this.formRegist.password
    }
    const toastRegister = Swal.mixin({
      title: '<strong>Mohon Tunggu</strong>',
      // text: 'Sedang dalam proses registrasi',
      timer: 3000,
    })
    let toastLogin;
    toastRegister.fire({
      onBeforeOpen: () => {
        // this.doLogin()
        toastRegister.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => {
        toastRegister.stopTimer();
        await localStorage.setItem("bulk_regist", JSON.stringify(form))
        await localStorage.setItem("bulk_login", JSON.stringify(formLogin))
        // this.events.publish('user:created', form);
        // this.navCrtl.navigateForward('apply-form')
        try {
          // await this.api.register(form).subscribe((resp: any) => {
          //   console.log(resp)
          //   if(resp.code == 409)  {
          //     Swal.fire({
          //       type: 'error',
          //       title: 'Oops...',
          //       text: resp.msg
          //     })
              
          //   } else { 
          //     toastLogin = Swal.mixin({
          //       title: '<strong>Mohon Tunggu</strong>',
          //       // text: 'Sedang dalam proses registrasi',
          //       timer: 3000,
          //     })
          //     toastLogin.fire({
          //       onBeforeOpen: () => {
          //         toastRegister.resumeTimer()
          //         toastLogin.showLoading();
          //         timerInterval = setInterval(() => {
          //         }, 3000)
          //       },
          //       onOpen: async () => {
          //         toastLogin.stopTimer();
          //         await this.api.login(formLogin).subscribe(async (res :any) => {
          //           let user = res[0];
          //           if(res.length > 0) {
          //             if(res[0]._id) {
          //               await localStorage.setItem("_USER", JSON.stringify({_ID: user._id, role:user.role, status:user.status}));
          //               await localStorage.setItem("_EMAIL", user.email)
          //             }
          //           } 
          //           console.log(res);
                    
          //         });
          //         await setTimeout(() => {
                    
          //         }, 3000)
          //         toastLogin.resumeTimer();
          //       },
          //       onClose: () => {
          //         clearInterval(timerInterval)
                  
          //       }
          //     })
          //   }
          // });
          // toastLogin.resumeTimer();
          toastRegister.resumeTimer()
        } catch (error) {
          alert(error)
        }
        
      },
      onClose: () => {
        clearInterval(timerInterval)
        this.navCrtl.navigateForward('apply-form')
      },
    })
  }

}
