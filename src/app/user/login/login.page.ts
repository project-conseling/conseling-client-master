import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/app/route.service';
import Swal from 'sweetalert2';
import { UserService } from '../user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  logo: any;
  formLogin = {
    email: '',
    password: ''
  }
  loading = false;
  constructor(private routeService: RouteService, private api: UserService, private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.logo = '../../../assets/images/logo-cons.png'
  }

  doLogin(){
    
    let timerInterval;
    Swal.fire({
      title: '<strong>Mohon Tunggu</strong>',
      text: 'Sedang dalam proses',
      timer: 3000,
      onBeforeOpen: () => {
        this.loading = true;
        Swal.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => {
        Swal.stopTimer();
        try {
          await this.api.login(this.formLogin).subscribe(async (res: any) => {
            let user = res;
            if(res && res.token) {
              if (user.id) {
                await localStorage.setItem("_USER", JSON.stringify({_ID: user.id, role:user.role, status:user.status}));
                await localStorage.setItem("_EMAIL", user.email)
              } 
            } else {
              Swal.fire('Oops...', 'Password atau email tidak sesuai!', 'error')
              this.loading = false;
            }
          })
          
        } catch (error) {
          Swal.fire('Oops...', error, 'error')
        }
        setTimeout(() => {
          Swal.resumeTimer();
        }, 2000)
       
      },
      onClose: () => {
        this.loading = false;
        if (localStorage.getItem("_USER")) {
          let next = this.route.snapshot.queryParams['next'] || 'home'
          // this.routeService.navigateTo("home")
          window.location.href = next
          // this.router.navigateByUrl(next)
        } 
        clearInterval(timerInterval)
      }
    })
  }

  questionCs() {
    this.router.navigateByUrl('cs-chat')
  }

}
