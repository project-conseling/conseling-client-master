import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment'
import { RTCService } from '../rtc.service';
const ENV = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private  httpClient:  HttpClient, private rtcService: RTCService) { }
  register(newUser: any) {
    // return this.httpClient.post(`${ENV}/regist`, newUser);
    return this.httpClient.post(`${ENV}/user/register`, newUser);
  }
  resetMyPassword(form, token) {
    return this.httpClient.post(`${ENV}/user/reset-password`, form, {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${token}`
      })
      
    });
  }

  forgetPassword(form) {
    return this.httpClient.post(`${ENV}/forget-password/email-checking`, form);
  }

  createProfile(newUser: any) {
    return this.httpClient.post(`${ENV}/user/profile`, newUser);
  }

  login(user: any) {
    let date = new Date()
    user = this.httpClient.post(`${ENV}/user/login`, user)
    user.subscribe(async (user: any) => {
      date = new Date(user.created_on)
      // console.log(date)
      await localStorage.setItem("rtcID", JSON.stringify({rtc_id: date.getTime()}))
    })
    return user
  }

  updateUser(form) {
    return this.httpClient.put(`${ENV}/user/me/${form.id}`, form)
  }
  getUser(id) {
    return this.httpClient.get(`${ENV}/user/me/`+id)
  }
  updateProfile(profile: any) {
    let form = profile;
    return this.httpClient.put(`${ENV}/profile/${profile._id}`, form)
  }
}
