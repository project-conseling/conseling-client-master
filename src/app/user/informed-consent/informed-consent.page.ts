import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from '../user.service';

@Component({
  selector: 'app-informed-consent',
  templateUrl: './informed-consent.page.html',
  styleUrls: ['./informed-consent.page.scss'],
})
export class InformedConsentPage implements OnInit {

  constructor(private modalCtrl: ModalController, private router: Router, private userApi: UserService) { }

  ngOnInit() {
  }

  iAgree() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    this.userApi.getUser(id).subscribe((res: any) => {
      res.data.status = 9;
      this.userApi.updateUser(res.data).subscribe(async (resp: any) => {
        localStorage.removeItem('_USER')
        let updated = JSON.parse(storeLocal)
        updated.status = 9;
        await localStorage.setItem('_USER', JSON.stringify(updated))
        // this.router.navigateByUrl('home')
        // this.modalCtrl.dismiss()
        window.location.href = "home"
      })
    })
    
  }
  openSwall() {
    Swal.fire({
      title: 'Anda Yakin ?',
      text: "Anda tidak dapat melanjutkan konseling jika tidak menyetujui informed consent",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'OK',
      cancelButtonText: 'KEMBALI',
    }).then(async (result) => {
      if (result.value) {
        await localStorage.clear()
        // await this.modalCtrl.dismiss()
        window.location.href = "login";
        // this.router.navigateByUrl('home')
      }
    })
  }
}
