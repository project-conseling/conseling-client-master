import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformedConsentPage } from './informed-consent.page';

describe('InformedConsentPage', () => {
  let component: InformedConsentPage;
  let fixture: ComponentFixture<InformedConsentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformedConsentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformedConsentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
