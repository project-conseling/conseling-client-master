import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from 'jquery'
import { Socket } from 'ng-socket-io';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { File as Files, FileEntry } from '@ionic-native/File/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';

import { ActionSheetController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { ProvidersService } from 'src/app/services/providers.service';
import { UserService } from '../user.service';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
@Component({
  selector: 'app-modify-profile',
  templateUrl: './modify-profile.page.html',
  styleUrls: ['./modify-profile.page.scss'],
})
export class ModifyProfilePage implements OnInit {
  formUpdate = {
    address: "",
    avatar: '',
    birth: "",
    gender: "men",
    hp: 98123874723,
    ktp: '',
    name: "",
    userId: "",
    __v: 0,
    _id: ""
  }

  bod: '';
  image: any;

  ktpName: ''
  fotoName: ''

  urlPhoto = ''
  urlKtp = ''
  idPhoto: any;
  idKtp: any;

  constructor(private api: ProvidersService, private router: Router,
    private userApi: UserService, private socket: Socket, private camera: Camera,
    private actionSheetCtrl: ActionSheetController,
    private clodinary: CloudinaryService) { }


  ngOnInit() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    this.api.getPatientComplaint(id).subscribe((res: any) => {
      this.formUpdate = res.profile
      console.log(res)
    })
  }

  

  
  normalizeDate() {
    let date = this.formUpdate.birth
    let dateChiper = date.split("T")
    let dateArr = dateChiper[0].split("-");
    let dateText;
    switch (dateArr[1]) {
      case "01":
        dateText = dateArr[2]+" Januari "+dateArr[0]
        break;
      case "02":
        dateText = dateArr[2]+" Februari "+dateArr[0]
        break;
      case "03":
        dateText = dateArr[2]+" Maret "+dateArr[0]
        break;
      case "04":
        dateText = dateArr[2]+" April "+dateArr[0]
        break;
      case "05":
        dateText = dateArr[2]+" Mei "+dateArr[0]
        break;
      case "06":
        dateText = dateArr[2]+" Juni "+dateArr[0]
        break;
      case "07":
        dateText = dateArr[2]+" Juli "+dateArr[0]
        break;
      case "08":
        dateText = dateArr[2]+" Agustus "+dateArr[0]
        break;
      case "09":
        dateText = dateArr[2]+" September "+dateArr[0]
        break;
      case "10":
        dateText = dateArr[2]+" Oktober "+dateArr[0]
        break;
      case "11":
        dateText = dateArr[2]+" November "+dateArr[0]
        break;
      case "12":
        dateText = dateArr[2]+" Desember "+dateArr[0]
        break;
      default:
        break;
    }
    this.bod = dateText;
  }
  doInputFile(element) {
    switch (element) {
      case 'foto':
        $('#input-avatar').click()
        break;
      case 'ktp':
        $('#input-ktp').click()
        break;
      default:
        break;
    }
  }

  openCalendar() {
    $('#cal-hide').click()
  }

  doUpdate() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    let statusActive = JSON.parse(storeLocal).status
    let formUpdate: any;
    const toastUpdate = Swal.mixin({
      title: '<strong>Mohon Tunggu</strong>',
      text: 'Sedang dalam proses update data',
      timer: 3000,
    })
    toastUpdate.fire({ 
      onBeforeOpen: () => {
        toastUpdate.showLoading()
        formUpdate = {
          address: this.formUpdate.address,
          birth: this.formUpdate.birth,
          gender: this.formUpdate.gender,
          avatar: this.urlPhoto,
          ktp: this.urlKtp,
          name: this.formUpdate.name,
          _id: this.formUpdate._id
        }
      },
      onOpen: () => {
        toastUpdate.stopTimer()
        this.userApi.updateProfile(formUpdate)
        .subscribe((res: any) => {
          this.userApi.getUser(id).subscribe((ress: any) => {
            ress.status = 4;
            this.userApi.updateUser(ress).subscribe(async (resp: any) => {
              this.socket.emit('patient-update')
              localStorage.removeItem('_USER')
              let updated = JSON.parse(storeLocal)
              updated.status = 4;
              await localStorage.setItem('_USER', JSON.stringify(updated))
              if(statusActive == 3) {
                window.location.href = "modify-form"
                // this.router.navigateByUrl('modify-form')
              } else {
                
                window.location.href = "home"
                // this.router.navigateByUrl('home')
              }
            })
          })
          
        })
      }
    })
    
    
    
  }


  async selectImage(imgFor: 'ktp' | 'pp') {
    const actionSheet = await this.actionSheetCtrl.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.PHOTOLIBRARY, imgFor);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.CAMERA, imgFor);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
}

  startUpload(imgEntry, imgFor: 'ktp' | 'pp') {
    // this.error = null;
    // this.loading = await this.loadingCtrl.create({
    //   message: 'Uploading...'
    // });

    // this.loading.present();

    window['resolveLocalFileSystemURL'](imgEntry,
      entry => {
        entry['file']((file: any) => {
          if(imgFor == 'ktp') {
            this.ktpName = file.name;
          } else if(imgFor == 'pp') {
            this.fotoName = file.name
          }
          this.readFile(file, imgFor)
        });
      });
  }
  async readFile(file: any, imgFor: 'ktp' | 'pp') {
    let folder;
    if(imgFor == 'ktp') {
      this.ktpName = file.name;
      folder = 'ktp'
    } else if(imgFor == 'pp') {
      this.fotoName = file.name
      folder = 'profile'
    }
    
      
    const reader = new FileReader();
    reader.onloadend = async () => {
      Swal.fire({
        title: 'Mohon tunggu',
        text: 'upload file',
        timer: 3000,
        onBeforeOpen: () => {
          Swal.showLoading();
          if(imgFor == 'ktp') {
            this.ktpName = file.name;
            folder = 'ktp'
          } else if(imgFor == 'pp') {
            this.fotoName = file.name
            folder = 'profile'
          }
        },
        onOpen: async () => {
          Swal.stopTimer();
          const formData = new FormData();
          const imgBlob = new Blob([reader.result], {type: file.type});
          formData.append('file', imgBlob, file.name);
          formData.append('folder', folder)
          formData.append('upload_preset', 'c2y6kxd3')
          await this.clodinary.upload(formData)
          .subscribe( async (resp: any) => {
          
            if(imgFor == 'ktp') {
              this.urlKtp = resp.url
              this.idKtp = resp.public_id
              this.formUpdate.ktp = resp.url
            } else if(imgFor == 'pp') {
              this.urlPhoto = resp.url
              this.idPhoto = resp.public_id
              this.formUpdate.avatar = resp.url
            }
          })
          await setTimeout(() => {
            Swal.resumeTimer()
          }, 3000)
        }
      })
      
      
      // this.postData(formData);
      
    };
    await reader.readAsArrayBuffer(file);
  }

  openCam(sourceType: PictureSourceType, imgFor: 'ktp' | 'pp'){

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: sourceType,
      mediaType: this.camera.MediaType.PICTURE
    }
    

    this.camera.getPicture(options).then((imageData) => {
     this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
     this.startUpload(imageData, imgFor);
    }, (err) => {
     // Handle error
     alert("error "+JSON.stringify(err))
    });

  }

}
