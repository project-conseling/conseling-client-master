import { Component, OnInit } from '@angular/core';
import { RouteService } from 'src/app/route.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(public routeService: RouteService) { }

  ngOnInit() {
  }

  async doLogout(){
    await localStorage.clear();
    this.routeService.navigateTo('login')
  }

}
