import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { CalendarModule } from 'ion2-calendar';
import { Camera } from '@ionic-native/camera/ngx';
import { ListConselingDirective } from './directives/list-conseling.directive';
import { ConselingScheduleDirective } from './directives/conseling-schedule.directive';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { ConselingHistoryDirective } from './directives/conseling-history.directive';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';

// const config: SocketIoConfig = {url: 'http://api.konselor.my.id', options: {}};
const config: SocketIoConfig = {url: 'https://conselings.herokuapp.com', options: {}};

@NgModule({
  declarations: [
    AppComponent, 
    ListConselingDirective, 
    ConselingScheduleDirective, ConselingHistoryDirective],
  entryComponents: [],
  imports: [
    BrowserModule,
    ReactiveFormsModule, 
    IonicModule.forRoot(), 
    SocketIoModule.forRoot(config),
    AppRoutingModule,
    CalendarModule,
    HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Deeplinks,
    LocalNotifications,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
