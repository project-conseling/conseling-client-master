import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appConselingHistory]'
})
export class ConselingHistoryDirective {

  constructor(public viewContainerRef: ViewContainerRef) { }

}
