export class MyProfile {
    _id: any;
    gender: string;
    name: string;
    avatar: string;
    ktp: string;
    hp: number;
    birth: string;
    address: string;
    userId: any;
    __v: number;
}