import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-form-question',
  templateUrl: './form-question.page.html',
  styleUrls: ['./form-question.page.scss'],
})
export class FormQuestionPage implements OnInit {
  formPertanyaan = {
    name: "",
    age: null,
    question: ""
  }
  constructor(private router: Router, private api: ProvidersService) { }

  ngOnInit() {
  }
  checkAge() {
    if(this.formPertanyaan.age < 0) {
      this.formPertanyaan.age = 0
    }
  }

  startChat() {
    const toastRegister = Swal.mixin({
      title: '<strong>Mohon Tunggu</strong>',
      timer: 3000,
    })
    const toastSuccess = Swal.mixin({
      title: 'Terima Kasih',
      text: 'Pertanyaan anda akan segera kami jawab',
      type: 'success'
    })
    let timerInterval
    toastRegister.fire({
      onBeforeOpen: () => {
        // this.doLogin()
        toastRegister.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => { 
        toastRegister.stopTimer();
        await this.api.postQuestion(this.formPertanyaan)
        .subscribe(resp => {
          console.log(resp)
        })
        toastRegister.resumeTimer()
      },
      onAfterClose: () => {
        toastSuccess.fire({
          onAfterClose: () => {
            this.router.navigateByUrl('cs-chat')
          }
        })
      }
    })
    
  }

  doBack() {
    this.router.navigateByUrl('cs-chat')
  }

}
