import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormQuestionPage } from './form-question.page';

describe('FormQuestionPage', () => {
  let component: FormQuestionPage;
  let fixture: ComponentFixture<FormQuestionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormQuestionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormQuestionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
