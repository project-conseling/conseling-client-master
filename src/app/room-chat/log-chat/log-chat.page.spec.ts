import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogChatPage } from './log-chat.page';

describe('LogChatPage', () => {
  let component: LogChatPage;
  let fixture: ComponentFixture<LogChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
