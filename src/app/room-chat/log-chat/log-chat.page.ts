import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { IonContent, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-log-chat',
  templateUrl: './log-chat.page.html',
  styleUrls: ['./log-chat.page.scss'],
})
export class LogChatPage implements OnInit {
  @ViewChild('IonContent') content: IonContent
  @Input() msgList: any = [];
  @Input() toUser: any
  User: any
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.User = id
  }

  closeModal() {
    this.modalCtrl.dismiss()
  }

}
