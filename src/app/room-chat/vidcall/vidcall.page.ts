import { Component, OnInit } from '@angular/core';
import { RTCService } from 'src/app/rtc.service';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-vidcall',
  templateUrl: './vidcall.page.html',
  styleUrls: ['./vidcall.page.scss'],
})
export class VidcallPage implements OnInit {

  user;

  onload: boolean = false;
  constructor(public rtcService: RTCService, private api: ProvidersService) { }

  ngOnInit() {
    console.log(this.rtcService.getSession())
    this.user = this.rtcService.getUser()
    console.log(this.user)
  }


  answere() {
    this.rtcService.acceptCall()
  }

  decline() {
    this.rtcService.decline()
  }

  refuse() {
    this.rtcService.refuse()
  }

  getState() {
    if(this.rtcService.onstate == 'incoming') {
      return "Panggilan Masuk"
    } else if(this.rtcService.onstate == 'call') {
      return "Menghubungkan"
    } else if(this.rtcService.onstate == 'accept') {
      return "Terhubung"
    }
  }

}
