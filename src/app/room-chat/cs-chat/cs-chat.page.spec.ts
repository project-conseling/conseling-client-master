import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsChatPage } from './cs-chat.page';

describe('CsChatPage', () => {
  let component: CsChatPage;
  let fixture: ComponentFixture<CsChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsChatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
