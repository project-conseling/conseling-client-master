import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-cs-chat',
  templateUrl: './cs-chat.page.html',
  styleUrls: ['./cs-chat.page.scss'],
})
export class CsChatPage implements OnInit {

  constructor(private router: Router, private api: ProvidersService) { }
  beforeChat: boolean = true
  onLoad: boolean = true
  listQuestion = []

  skeletonLoad = [1,2,3,4,5,6,7,8,9,10,11,12]

  formSearch = ''
  async ngOnInit() {
    this.listQuestion = []
    try {
      await this.initialFunction()
    } catch (error) {
      
    } finally {
      setTimeout(() => {
        this.onLoad = false
      }, 2000)
    }
  }

  initialFunction() {
    this.onLoad = true
    this.api.getListQuestion()
    .subscribe((resp: any) => {
      if(resp && resp.length > 0) {
        resp.map((quest, index) => {
          if(quest.answered != null && index != 10) {
            this.listQuestion.push(quest)
          }
        })
      }
    })
  }

  startChat() {
    this.beforeChat = false;
  }

  doBack() {
    this.router.navigateByUrl('login')
  }

  
  createQuestion() {
    this.router.navigateByUrl('/form-question')
  }

  searchQuestion() {
    console.log(this.formSearch)
    if(this.formSearch != '') {
      this.listQuestion = []
      this.api.searchQuestion(this.formSearch)
      .subscribe((resp: any) => {
        console.log(resp)
        if(resp && resp.length > 0) {
          resp.map((quest, index) => {
            if(quest.answered != null ) {
              this.listQuestion.push(quest)
            }
          })
        }
      })
    } else {
      this.ngOnInit()
    }
  }



}
