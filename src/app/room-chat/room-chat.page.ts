import { Component, OnInit, ViewChild } from '@angular/core';
import { ProvidersService } from '../services/providers.service';
import { IonContent } from '@ionic/angular';
import { Complaint } from '../_types/complaint';
import { RouteService } from '../route.service';
import { RealUpdateService } from '../real-update.service';
import Swal from 'sweetalert2';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs';
import { RTCService } from '../rtc.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-room-chat',
  templateUrl: './room-chat.page.html',
  styleUrls: ['./room-chat.page.scss'],
})
export class RoomChatPage implements OnInit {
  @ViewChild('IonContent') content: IonContent
  dataComplaint: Complaint
  dataConseling: any;
  idConseling: any;
  msgList: any = [];
  onload: boolean = false;
  paramData: any;
  userName: any;
  actived_chat: boolean = false;
  user_input: string = "";
  User: string ;
  toUser: string ;
  start_typing: any;
  loader: boolean;
  profile: any

  chatter: boolean = false;

  methode = ''
  consultation: any
  activedToUser = false

  constructor(private api: ProvidersService, public routeService: RouteService, public socketApi: RealUpdateService,
    private socket: Socket, public rtcService: RTCService) {}

  async ngOnInit() {
    this.onload = true;
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.User = id

    await this.api.getPatientConseling(id)
    .subscribe((resp: any) => {
      let myConsultation
      if(resp && resp.length > 0) {
        myConsultation = resp[resp.findIndex(consult => consult.result == null || consult.result == '')]
      }
      this.methode = myConsultation.methode
      console.log("my c",myConsultation)
      this.consultation = myConsultation
      if(myConsultation) {
        this.toUser = myConsultation.conselor.id
        if(myConsultation.chat_rooms && myConsultation.chat_rooms.length > 0) {
          myConsultation.chat_rooms.forEach(chat => {
            let chipTime = chat.createdAt.split('T')[1].split(":")[0]+ ":" + chat.createdAt.split('T')[1].split(":")[1]
            console.log(chipTime)
            let chatData = {
              userId: chat.user_id,
              userName: chat.user.profile.name,
              userAvatar: chat.user.profile.avatar,
              time: chipTime,
              message: chat.chat,
              isReaded: 1,
              id: chat.id
            }
            this.msgList.push(chatData)
          });
        }
      }
      this.socketApi.getActiveUser().subscribe((dataObs: any) => {
        if(dataObs.user == this.toUser && dataObs.event == "joined") {
          this.activedToUser = true
        } else if(dataObs.user == this.toUser && dataObs.event == "left"){
          this.activedToUser = false
        }
      })
      
      
      // let dateNow = new Date()
      // let now = dateNow.getFullYear() +"-"+ this.pad((dateNow.getMonth()+1), 2) + "-" + this.pad(dateNow.getDate(), 2)
      // if(myConsultation) {
        
      // }
    })
    this.scrollDown()
    this.onload = false
    this.statusActive()

    this.getMessages()
    .subscribe(async (dataObs: any) => {
      await this.fetchData(dataObs)
      this.scrollDown()
    })

    
    
  }

  startVidCall(id) {
    this.rtcService.pushCall(id)
    this.routeService.navigateTo('vidcall')
  }
  startCall(id) {
    this.rtcService.pushCallAudio(id)
    this.routeService.navigateTo('caller')
    // this.modalCtrl.dismiss({state: "makeCall", to: id})
    // console.log(id)
  }
  statusActive() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.socket.emit('set-nickname', id)
    setTimeout(() => {
      this.statusActive()
    }, 3000)
  }

  sendMsg() {


    // let time = new Date()
    // let tempTime = this.pad(time.getHours(),2)+":"+this.pad(time.getMinutes(),2)
    // let formMsg = {
    //   complaint_id: this.idConseling,
    //   user_id: this.dataConseling.patientId,
    //   avatar: this.profile.avatar,
    //   name: this.dataConseling.patient.name,
    //   text: this.user_input,
    //   time: tempTime
    // }
    if (this.user_input !== '') {
      this.api.sendChat({
        conseling_id: this.consultation.id,
        chat: this.user_input,
        user_id: this.User,
        isReaded: 0
      })
      .subscribe((resp: any) => {
        console.log(resp)
        this.socket.emit('add-message', 
        {
          data: resp
        })
      })
      this.user_input = "";
      this.scrollDown()
      // setTimeout(() => {
      //   // this.senderSends()
      // }, 500);
    }
  }

  senderSends(data: any) {
    this.loader = true;
    console.log(data)
    setTimeout(() => {
      this.msgList.push({
        userId: this.User,
        userName: this.User,
        userAvatar: data.userAvatar,
        time: data.time,
        message: data.message
      });
      this.loader = false;
      this.scrollDown()
    }, 2000)
    this.scrollDown()
  }
  getMessages() {
    let observable = new Observable(obs => {
      this.socket.on('message', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }
  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(50)
    }, 50);
  }

  pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  fetchData(dataObs) {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.User = id
    this.api.getPatientConseling(id)
    .subscribe((resp: any) => {
      let myConsultation
      if(resp && resp.length > 0) {
        myConsultation = resp[resp.findIndex(consult => consult.result == null || consult.result == '')]
      }
      console.log(myConsultation)
      this.consultation = myConsultation
      if(myConsultation) {
        this.toUser = myConsultation.conselor.id
        if(myConsultation.chat_rooms && myConsultation.chat_rooms.length > 0) {
          myConsultation.chat_rooms.forEach(chat => {
            let chipTime = chat.createdAt.split('T')[1].split(":")[0]+ ":" + chat.createdAt.split('T')[1].split(":")[1]
            console.log(chipTime)
            let chatData = {
              userId: chat.user_id,
              userName: chat.user.profile.name,
              userAvatar: chat.user.profile.avatar,
              time: chipTime,
              message: chat.chat,
              isReaded: 1,
              id: chat.id
            }
            if(dataObs.data.id == chat.id) {
              this.msgList.push(chatData)
            }
          });
        }
      }
      // let dateNow = new Date()
      // let now = dateNow.getFullYear() +"-"+ this.pad((dateNow.getMonth()+1), 2) + "-" + this.pad(dateNow.getDate(), 2)
      // if(myConsultation) {
        
      // }
    })
  }

  checkSchedule(conseling) {
    console.log(conseling)
    
  }
  

}
