import { TestBed } from '@angular/core/testing';

import { RealUpdateService } from './real-update.service';

describe('RealUpdateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RealUpdateService = TestBed.get(RealUpdateService);
    expect(service).toBeTruthy();
  });
});
