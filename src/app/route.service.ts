import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouteService {

  constructor(private router: Router) { }

  navigateTo(page) {
    if(page == "home") {
      window.location.href = page
    }
    this.router.navigateByUrl(page)
  }
}
