import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { StatusGuard } from './guards/status.guard';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthGuard, StatusGuard]},
  { path: 'login', loadChildren: './user/login/login.module#LoginPageModule' },
  { path: 'forget-password', loadChildren: './user/forget-password/forget-password.module#ForgetPasswordPageModule' },
  { path: 'logout', loadChildren: './user/logout/logout.module#LogoutPageModule' },
  { path: 'modify-profile', loadChildren: './user/modify-profile/modify-profile.module#ModifyProfilePageModule' },
  { path: 'register', loadChildren: './user/register/register.module#RegisterPageModule' },
  { path: 'waiting-status', loadChildren: './user/waiting-status/waiting-status.module#WaitingStatusPageModule' },
  { path: 'room-chat', loadChildren: './room-chat/room-chat.module#RoomChatPageModule' },
  { path: 'apply-form', loadChildren: './complaint/apply-form/apply-form.module#ApplyFormPageModule' },
  { path: 'informed-consent', loadChildren: './user/informed-consent/informed-consent.module#InformedConsentPageModule' },
  { path: 'modify-form', loadChildren: './complaint/modify-form/modify-form.module#ModifyFormPageModule' },
  { path: 'caller', loadChildren: './room-chat/caller/caller.module#CallerPageModule' },
  { path: 'vidcall', loadChildren: './room-chat/vidcall/vidcall.module#VidcallPageModule' },
  { path: 'cs-chat', loadChildren: './room-chat/cs-chat/cs-chat.module#CsChatPageModule' },
  { path: 'form-question', loadChildren: './room-chat/form-question/form-question.module#FormQuestionPageModule' },
  { path: 'forget', loadChildren: './user/reset-password/reset-password.module#ResetPasswordPageModule' },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
